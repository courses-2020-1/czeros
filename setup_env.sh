#!/bin/bash

echo "[+] Setting up your enviroment."
CZEROS_ENV="czeros"
BASE_DIR=$PWD
echo "[+] Installing virtualenv."
pip3 install virtualenv

echo "[+] Creating a python virtual enviroment(~/pyenv/$CZEROS_ENV)."
if [[ !(-d ~/pyenv) ]]; then
    mkdir ~/pyenv

fi

if [[ -d ~/pyenv/$CZEROS_ENV ]]; then
        echo "[*] You already have a $CZEROS_ENV virtual enviroment."
        read -p "Do you want overwrite this enviroment or rename?[o/r] " overenv
        while [[ $overenv != 'o' && $overenv != 'r' ]]; do
            read -p "Do you want overwrite this enviroment(case sensible) or rename?[o/r] " overenv
        done
        if [[ $overenv == "o" ]]; then
            rm -rf ~/pyenv/$CZEROS_ENV
            python3 -m venv ~/pyenv/$CZEROS_ENV
        else 
            read -p "Name for the enviroment: " CZEROS_ENV
            
            if [[ -d ~/pyenv/$CZEROS_ENV ]]; then
                echo "The enviroment $CZEROS_ENV already exist."
                read -p "Select a new name for your enviroment: " CZEROS_ENV
                while [[ -d ~/pyenv/$CZEROS_ENV ]]; do
                    read -p "Select a new name for your enviroment: " CZEROS_ENV
                done
            fi

            mkdir ~/pyenv/$CZEROS_ENV
            python3 -m venv ~/pyenv/$CZEROS_ENV
        fi
else
    python3 -m venv ~/pyenv/$CZEROS_ENV
fi

source ~/pyenv/$CZEROS_ENV/bin/activate

echo "[+] Cloning git repository."
git clone https://gitlab.com/glozanoa/uni20201
cd uni20201

echo "[+] Installing python package(czeros)"
pip3 install .

echo "[+] Installing javascript dependences."
NODE_DEPENDS=~/pyenv/$CZEROS_ENV/lib/python3.8/site-packages/czeros/py2js 
cd $NODE_DEPENDS && npm install .

#UNI20201=$BASE_DIR/uni20201
#cd $UNI20201

echo """
USAGE:

# Computer the zeros  and print them in the terminal also write them in a csv file(czeros.csv)

czeros -i PATH_INPUT_FILE 

# Computer the zeros  and print them in the terminal , write them in a csv file(czeros.csv) and plot them.

czeros -i PATH_INPUT_FILE --graph


EXAMPLE OF INPUT_FILE(without comments):
An example of this file is in czeros/doc/examples/ifile.in
[czeros]
f = z**2+z      # regla de correcpondencia de la funcion compleja  
z0 = [0,0]      # Punto central de disco  
r = 2           # Radio de disco   
tol = 1e-3      # Tolerancia de cálculo


If you find some bug report it to: glozanoa@uni.pe
"""