Write-Host "[+] Setting up your enviroment."
$ENV_NAME="czeros"
$BASE_DIR=Get-Location

Write-Host "[+] Creating a python virtual enviroment($HOME_DIR/pyenv/$CZEROS_ENV)."

$PYENV_DIR = Join-Path $HOME 'pyenv'
$ENV_DIR = Join-Path $PYENV_DIR $ENV_NAME
if (Test-Path $PYENV_DIR) {
    if(Test-Path $ENV_DIR)
    {
        Write-Host "[*] You already have a $CZEROS_ENV virtual enviroment."
        $OVER_ENV = Read-Host "Do you want overwrite this enviroment or rename?[o/r] "
        while ($OVER_ENV -ne "o" -or $OVER_ENV -ne "r") {
            $OVER_ENV = Read-Host "Do you want overwrite this enviroment or rename?(case sensible)[o/r] "
        }

        if($OVER_ENV -eq "o"){
            Remove-Item -Force $ENV_DIR
            mkdir $ENV_DIR
            python -m venv $ENV_DIR
        }
        else {
            $ENV_NAME = Read-Host "Name for the enviroment: "
            $ENV_DIR = Join-Path $PYENV_DIR $ENV_NAME
            if(Test-Path $ENV_DIR){
                Write-Host "[-] Enviroment $ENV_NAME already exist!."
                $ENV_NAME = Read-Host "Name for the enviroment: "
                $ENV_DIR = Join-Path $PYENV_DIR $ENV_NAME

                while (Test-Path $ENV_DIR) {
                    Write-Host "[-] Enviroment $ENV_NAME already exist!."
                    $ENV_NAME = Read-Host "Name for the enviroment: "
                    $ENV_DIR = Join-Path $PYENV_DIR $ENV_NAME
                }
                python -m venv $ENV_DIR
            }
        }
    }
    python -m venv $ENV_DIR
}
else {
    mkdir $PYENV_DIR
    python -m venv $ENV_DIR
}


Write-Host "[+] Enabling $ENV_NAME enviroment."
$ACTIVE_ENV = Join-Path $ENV_DIR '\Scripts\Activate.ps1'
.$ACTIVE_ENV

Write-Host "[+] Cloning git repository."
git clone https://gitlab.com/glozanoa/uni20201

$REPO_DIR = Join-Path $BASE_DIR "uni20201"

Set-Location $REPO_DIR

Write-Host "[+] Installing czeros package."
pip install .

Write-Host "[+] Installing javascript dependences."

$NODE_DEPENDS= Join-Path $ENV_DIR "lib/python3.8/site-packages/czeros/py2js"

Set-Location $NODE_DEPENDS && npm install .

Write-Host """
USAGE:

# Computer the zeros  and print them in the terminal also write them in a csv file(czeros.csv)

czeros -i PATH_INPUT_FILE 

# Computer the zeros  and print them in the terminal , write them in a csv file(czeros.csv) and plot them.

czeros -i PATH_INPUT_FILE --graph


EXAMPLE OF INPUT_FILE(without comments):
An example of this file is in czeros/doc/examples/ifile.in
[czeros]
f = z**2+z      # regla de correcpondencia de la funcion compleja  
z0 = [0,0]      # Punto central de disco  
r = 2           # Radio de disco   
tol = 1e-3      # Tolerancia de cálculo


If you find some bug report it to glozanoa@uni.pe or add an issue to the gitlab repository.
"""