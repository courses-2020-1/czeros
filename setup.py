from setuptools import setup
def readme():
    with open('README.md', 'r') as doc:
        return doc.read()    

setup(
    name="czeros",
    version="5.4",
    description="Compute zeros of analitic functions.",
    long_description=readme(),
    classifiers=[
        'Programming Language :: Python :: 3.8',
    ],
    keywords="Complex Analysis Analytic Functions",
    url = "https://gitlab.com/glozanoa/uni20201",
    author=[
        "Mirian Geronimo",
        "Carlos Mundo",
        "clcuc",  
        "glozanoa",
    ],
    author_email="glozanoa@uni.pe",
    license="GPL3",
    packages= ['czeros'],
    include_package_data=True,
    package_data={
        'czeros':["py2js/*"],
        #'czeros':["py2js/node_modules/*"],
    },
    install_requires=[
        'wheel',
        'sympy',
        'javascripthon',
        'optparse-pretty',
        'configparser',
        'matplotlib'
    ],
    #scripts=["czeros/bin/czeros"],
    entry_points={
        "console_scripts":[
            "czeros = czeros.executable:main",
        ],
    },
    zip_safe=True
)
