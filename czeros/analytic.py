#!/usr/bin/env python3

import os
from os.path import dirname
import subprocess
from configparser import ConfigParser
import sympy
from sympy import I

import re
import numpy as np
import matplotlib.pyplot as plt
#import pandas as pd


class PkgStruct:
    def __init__(self, file=__file__):
        self.abs_pfile = os.path.abspath(file)
        self.pkg = dirname(self.abs_pfile)
        self.py2js =  self.pkg + "/py2js"

class Cfunction:

    def __init__(self, rule):
        self.z = sympy.symbols('z') # z = x+iy: complex number
        self.x, self.y = sympy.symbols('x y', real=True)

        self.rule = rule # rule = f(z)
        self.f = None # f(z) = [re(f), im(f)]
        self.fp = None

    def cf(self):
        fz = sympy.sympify(self.rule)
        fz = fz.subs(self.z, self.x+I*self.y)
        fz = fz.expand()
        self.f = fz.as_real_imag()
        return self.f

    def cdf(self):
        # Calcula la derivada de la funcion compleja f
        re_df = sympy.diff(self.f[0], self.x)
        img_df = sympy.diff(self.f[1], self.x)
        self.fp = (re_df, img_df)
        return self.fp


class Translate:
    def __init__(self,**kwargs):
        self.z0 = kwargs['z0']
        self.r = kwargs['r']
        self.tol = kwargs['tol']
        # complex function (z variable)
        self.cfunc = Cfunction(kwargs['f'])
        self.czeros_struct = PkgStruct()

    def write_py(self):
        f = self.cfunc.cf()
        fp = self.cfunc.cdf()

        fstr =  "def f(out, x, y):"+\
                "\n\tout[0] = {}".format(f[0]) +\
                "\n\tout[1] = {}".format(f[1])

        fpstr = "\n\ndef fp(out, x, y):"+\
                "\n\tout[0] = {}".format(fp[0]) +\
                "\n\tout[1] = {}".format(fp[1])

        args = "\n\nz0 = {}\nr={}\ntol={}".format(self.z0, self.r, self.tol)

        pyczeros_path = self.czeros_struct.py2js + "/czeros.py"

        pyczeros = open(pyczeros_path, 'w')
        pyczeros.write(fstr)
        pyczeros.write(fpstr)
        pyczeros.write(args)
        pyczeros.close()

    def write_js(self):
        self.write_py()
        pyczeros_path = self.czeros_struct.py2js + "/czeros.py"
        subprocess.run(["pj", pyczeros_path])
        jsczeros_path = self.czeros_struct.py2js + "/czeros.js"
        jsczeros = open(jsczeros_path, 'a')

        jsczeros.write("\n\nvar delvesLyness = require('./delvesLyness')\n")
        jsczeros.write("let fczeros = delvesLyness(f, fp, z0, r, tol)\n")
        jsczeros.write("console.log(fczeros)\n")
        jsczeros.close()


class Czeros:
    def __init__(self, ifile, cdefun=None):
        self.ifile = ifile
        #self.ofile = ofile
        self.py2js_translate = None
        self.czeros_struct = PkgStruct()
        self.js = self.czeros_struct.py2js + "/czeros.js"
        self.py = self.czeros_struct.py2js + "/czeros.py"
        self.czeros_csv = self.czeros_struct.py2js + "/czeros.txt"
        self.czeros = None
        self.cdefun = cdefun

    def read_param(self):
        try:
            cfg = ConfigParser()
            cfg.read(self.ifile)

            #import pdb
            #pdb.set_trace()

            if 'czeros' not in cfg.sections():
                print("No czeros section!")
                exit(1)
            self.param = cfg._sections['czeros']
            
            if 'f' in self.param and self.cdefun == None:
                self.py2js_translate = Translate(**self.param)
            
            elif 'f' not in self.param and self.cdefun == None:
                print("[-] There isn't a Complex function!")

            elif 'f' not in self.param and self.cdefun != None:
                try:
                    #cdefun = self.cdefun
                    #import pdb
                    #pdb.set_trace()
                    from cdefun import f
                    self.param['f'] = f
                    self.py2js_translate = Translate(**self.param)
                except ImportError as err:
                    print(f"[-] Module {self.cdefun} doesn't exist.\nERROR:{err}")

        except FileNotFoundError:
            print("No input file!")
            exit(1)


    def zeros(self):
        self.read_param()
        self.py2js_translate.write_js()
        subprocess.run(['node', self.js])
        self.czeros = np.genfromtxt("czeros.csv", delimiter=",")

        # Printing the zeros
        #print("\tRE\tIM")
        #for idx, re, im in enumerate(self.czeros[0], self.czeros[0]):
        #    print("{}\t{}\t{}".format(idx, re, im))

    
    def graphic(self):
        self.zeros()
        print("[+] Ploting zeros.")
        radio = float(self.param["r"])

        z0 = re.match(r"\[(-?\d*\.?\d*),\s*(-?\d*\.?\d*)\]", self.param["z0"])

        center = np.array(z0.groups(), dtype=np.float64)


        # ploting circunference and zeros
        theta = np.linspace(0, 2*np.pi, 100)

        x = center[0] + radio*np.cos(theta)
        y = center[1] + radio*np.sin(theta)

        plt.xlim(center[0]-3*radio/2, center[0]+3*radio/2)
        plt.ylim(center[1]-3*radio/2, center[1]+3*radio/2)
        plt.grid(linestyle="--")
        
        plt.scatter(center[0], center[0], label="z0")
        plt.plot(x, y, label="Dr(z0)")
        
        #zeros = np.genfromtxt("czeros.csv", delimiter=",")

        plt.scatter(self.czeros[0], self.czeros[1], label="Complex zeros")

        plt.title("Complex Zeros of {}".format(self.param["f"]))
        plt.legend()
        plt.show()
        