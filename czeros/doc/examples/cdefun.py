import sympy


z = sympy.symbols('z')

f = z**2*(z-2)**2*(sympy.exp(2*z)*sympy.cos(z)+z**3-1-sympy.sin(z))

