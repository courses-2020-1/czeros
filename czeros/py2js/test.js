var delvesLyness = require('./delvesLyness')

//complex function f(z) = (z+1)(z-I)

function f(out, x, y)
{
    out[0] = Math.pow(x, 2) - Math.pow(y, 2) - x + y
    out[1] = 2*x*y - x - y + 1 
}

function fp(out, x, y)
{
    out[0] = 2*x-1
    out[1] = 2*y-1
}

z0 = [0, 0]
r = 2

let fczeros = delvesLyness(f, fp, z0, r)

//let fs = require('fs')
console.log(fczeros)
//fs.writeFileSync("/home/solalu/cryptoana/devel/czeros/czeros/jsczeros/test_output.txt", fczeros)
//console.log("File written successfully")