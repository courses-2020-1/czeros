'use strict';

var locateZeros = require('./locate_zeros');

module.exports = delvesLyness;


const fsLib = require('fs')
function write_czeros(czeros_file, czeros){
  var czeros_str = "";

  czeros.forEach(element => {
    czeros_str += element + '\n';
  });
  
  fsLib.writeFile(czeros_file, czeros_str, 
    function(err){
      if(err){
        return console.error(err);
      }
  }
  );
}

function delvesLyness (f, fp, z0, r, tol, maxDepth) {
  z0 = z0 === undefined ? [0, 0] : z0;
  r = r === undefined ? 1 : r;
  maxDepth = maxDepth === undefined ? 20 : maxDepth;
  tol = tol === undefined ? 1e-8 : tol;

  var czeros = locateZeros([], [], f, fp, z0[0], z0[1], r, z0[0], z0[1], r, tol, maxDepth, 0);

  // write czeros in a file
  var czeros_file = "czeros.csv"
  write_czeros(czeros_file, czeros);
  console.log("[+] Complex Zeros saves in file "+ czeros_file)
  return czeros;
}
