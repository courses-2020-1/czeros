var r, tol, z0;
function f(out, x, y) {
    out[0] = ((Math.pow(x, 2) + x) - Math.pow(y, 2));
    out[1] = (((2 * x) * y) + y);
}
function fp(out, x, y) {
    out[0] = ((2 * x) + 1);
    out[1] = (2 * y);
}
z0 = [0, 0];
r = 2;
tol = 0.001;

//# sourceMappingURL=czeros.js.map


var delvesLyness = require('./delvesLyness')
let fczeros = delvesLyness(f, fp, z0, r, tol)
console.log(fczeros)
