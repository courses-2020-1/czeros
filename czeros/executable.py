#!/usr/bin/env python3

import subprocess
import optparse
import sys
import os


def main():
    exec_name = os.path.basename(sys.argv[0])
    
    empty_cmd = """{} -i <input file> --graph\n
    ########### input struct ###########
    #
    #   [czeros]
    # 
    #   f = REGLA DE CORRESPONDENCIA DE FUNCION COMPLEJA
    #   z0 = PUNTO CENTRAL DEL DISCO
    #   r = RADIO DEL DISCO 
    #   tol = TOLERANCIA DE CALCULO
    #  
    #####################################
    """.format(exec_name)
    
    parser = optparse.OptionParser(empty_cmd)
    
    parser.add_option('-i', dest='ifile', type='string', help='Input arguments')
    parser.add_option('-g','--graph', action="store_true",dest="graph", default=False, help='Enable graphic mode')
    
    (options, args) = parser.parse_args()
    
    if(options.ifile == None):
        print(parser.usage)
        sys.exit(1)
    else:
        if not os.path.isfile(options.ifile):
            print(f"[-]File {options.ifile} don't exist!")
            sys.exit(1)
        else:
            ifile = options.ifile
            #ofile = options.ofile        
            
            import czeros.analytic as analytic
            czeros = analytic.Czeros(ifile)
            if options.graph:
                czeros.graphic()
            else:
                czeros.zeros()