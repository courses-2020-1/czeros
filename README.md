# Zeros Complejos de funciones analiticas

## Installation
NOTA: Debe tener instalado `node`.
```bash
wget https://gitlab.com/glozanoa/uni20201/-/raw/master/setup_env.sh
chmod +x setup_env.sh
./setup_env.sh
```

##### NOTA:
> Siempre que uses este paquete activa el enviroment creado.
```bash
source ~/pyenv/czeros/bin/activate
```

## Usage
#### Input file (`sin comentarios`)
```bash
[czeros]
f = z**2+z      # regla de correcpondencia de la funcion compleja  
z0 = [0,0]      # Punto central de disco  
r = 2           # Radio de disco   
tol = 1e-3      # Tolerancia de cálculo
```


#### Calculo de Zeros complejos
**NOTE**: There is a for testing it(`czeros/doc/examples/ifile.in`)
* Usando `cli`

  - Without plot
    Computer the zeros  and print them in the terminal also write them in a csv file(czeros.csv)
    ```bash
    czeros -i PATH_INPUT_FILE 
    ```

    Using `czeros/doc/examples/ifile.in`  
    ![czeros_cli](czeros/doc/examples/czeros_graph.png)  
    
  - With plot
    Computer the zeros  and print them in the terminal , write them in a csv file(czeros.csv) and plot them.
    ```bash
    czeros -i PATH_INPUT_FILE --graph 
    ```
    Using `czeros/doc/examples/ifile.in`  
    ![czeros_cli_graph](czeros/doc/examples/czeros_graph.png)  
    ![czeros_plot](czeros/doc/examples/czeros_graph_plot.png)  

* Usando la clase `Czeros`
Start ipython, then:  

```bash
from czeros.analytic import Czeros
test = Czeros(PATH_INPUT_FILE)
# without plot
test.zeros()

# whith plot
test.graphic()
```
